<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => '/api/v1',
], function (Router $router) {
    $router->group([
        'prefix' => '/words',
    ], function (Router $router) {
        $router->get('/random-word',
            [\App\Http\Controllers\Api\WordsController::class, 'randomWord']
        );

        $router->get('',
            [\App\Http\Controllers\Api\WordsController::class, 'words']
        );
    });
});
