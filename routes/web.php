<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/dashboard', function () {
    return view('dashboard');
});


//Route::get('/guess-word', [\App\Http\Controllers\GuessWordsController::class, 'index'])
//    ->name('guess-word');

Route::get('/{userId}/courses', [\App\Http\Controllers\CoursesController::class, 'index'])
    ->name('courses');

Route::get('/courses/{courseId}', [\App\Http\Controllers\CoursesController::class, 'lessons'])
    ->name('course.lessons');

Route::get('/courses/{courseId}/{lessonId}/{lessonPartId?}', [\App\Http\Controllers\CoursesController::class, 'lesson'])
    ->name('course.lesson.parts');


Auth::routes();

// DICTIONARIES ROUTES

Route::get('/dictionary', function () {
    return view('dictionary.myDictionaries');})
    ->name('dictionary')
    ->middleware('auth');

Route::post('/dictionary/submit',[\App\Http\Controllers\DictionaryController::class, 'create'])
    ->name('dictionary-submit');

Route::get('/dictionary/all', [\App\Http\Controllers\DictionaryController::class, 'allData'])
    ->name('dictionary-data')
    ->middleware('auth');

Route::get('/dictionary/all/{id}', [\App\Http\Controllers\DictionaryController::class, 'showOneDictionary'])
    ->name('dictionary-data-one');

Route::post('/dictionary/all/update/{id}', [\App\Http\Controllers\DictionaryController::class, 'update'])
    ->name('dictionary-update');

Route::get('/dictionary/all/delete/{id}', [\App\Http\Controllers\DictionaryController::class, 'delete'])
    ->name('dictionary-delete');

Route::get('/guess-word/{id}', [\App\Http\Controllers\GuessWordsController::class, 'index'])
    ->name('guess-word');

// MEMORIZE-WORD ROUTES

Route::get('/memorize-word', [\App\Http\Controllers\MemorizeWordController::class, 'index'])
    ->name('memorize-word');

// HOME ROUTE

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('home');
