<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WordsController extends Controller
{
    public function randomWord()
    {
        /** @var Word $word */
        $word = Word::where('language_id', 1)->inRandomOrder()->with(['translations', 'examples'])->first();
        $wordArray = $word->attributesToArray();
        if ($word->translations()->exists()) {
            $wordArray['translations'] = $word->getRelation('translations')->makeHidden('pivot')->toArray();
        }

        if ($word->examples()->exists()) {
            $wordArray['examples'] = $word->getRelation('examples')->toArray();
        }

        return json_encode($wordArray);
    }

    public function words()
    {
        /** @var Word $word */
        $words = Word::where('language_id', 1)->inRandomOrder()->with('translations')->get();
        $wordsArray = [];
        foreach ($words as $word) {
            $wordArray = $word->attributesToArray();
            if ($word->translations()->exists()) {
                $wordArray['translations'] = $word->getRelation('translations')->makeHidden('pivot')->toArray();
            }
            $wordsArray[] = $wordArray;
        }

        return json_encode($wordsArray);
    }
}
