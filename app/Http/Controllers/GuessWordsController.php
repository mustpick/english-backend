<?php

namespace App\Http\Controllers;

use App\Models\Dictionary;
use App\Models\Image;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GuessWordsController extends Controller
{

    public function index($id) {
        return view('guess-word.index',[
            'word' => Dictionary::where('user_id', '=', Auth::id())
                ->where('id', '=', $id)
                ->first()
                ->random_word()
        ]);
    }
}
