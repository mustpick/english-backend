<?php

namespace App\Http\Controllers;

use App\Models\Dictionary;
use App\Http\Requests\DictionaryRequest;
use App\Models\User;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class DictionaryController extends Controller
{

    public function create(DictionaryRequest $request)
    {
        $dictionary = new Dictionary();
        $dictionary->dictionary_name = $request->input('name');
        $dictionary->user_id = Auth::user()->id;
        $dictionary->save();

        return redirect()->route('dictionary-data');
    }

    public function update($id ,DictionaryRequest $request)
    {
        $dictionary = Dictionary::find($id);
        $dictionary->dictionary_name = $request->input('name');
        $dictionary->save();

        return redirect()->route('dictionary-data');
    }

    public function delete($id){
        Dictionary::find($id)->delete();

        return redirect()->route('dictionary-data');
    }

    public function allData(){
        return view('dictionary.myDictionaries', [
            'data' => Dictionary::where('user_id', '=', Auth::id())->get()
        ]);
    }

    public function showOneDictionary($id){
        return view('dictionary.one-dictionary', [
            'dictionary' => Dictionary::where('user_id', '=', Auth::id())->where('id', '=', $id)->first()
        ]);
    }
}
