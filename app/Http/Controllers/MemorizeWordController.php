<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Illuminate\Http\Request;

class MemorizeWordController extends Controller
{
    //
    public function index(){
        return view('memorize-word.memorize-word', [
           'wordAndAnswer' => [ Word::where('language_id', 1)
               ->inRandomOrder()
               ->with('transcriptions', 'translations', 'image')
               ->first(),
           Word::where('language_id', 1)
                ->inRandomOrder()
                ->with('transcriptions', 'translations', 'image')
                ->first(),
           Word::where('language_id', 1)
                ->inRandomOrder()
                ->with('transcriptions', 'translations', 'image')
                ->first(),
           Word::where('language_id', 1)
                ->inRandomOrder()
                ->with('transcriptions', 'translations', 'image')
                ->first()

        ]]);
    }
}
