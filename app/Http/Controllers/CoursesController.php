<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Lesson;
use App\Models\LessonPart;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function index($userId)
    {
        $user = User::find($userId);
        if (!$user)
            throw new ModelNotFoundException();

        return view('courses.index', [
            'courses' => $user->courses
        ]);
    }

    public function lessons($courseId)
    {
        $course = Course::find($courseId);
        if (!$course)
            throw new ModelNotFoundException();

        return view('courses.lessons', [
            'course' => $course
        ]);
    }

    public function lesson($courseId, $lessonId, $lessonPartId = null)
    {
        $lesson = Lesson::find($lessonId);
        if (!$lesson)
            throw new ModelNotFoundException();

        if (!$lessonPartId)
            $mainPart = LessonPart::where('lesson_id', $lesson->id)->first();
        else
            $mainPart = LessonPart::find($lessonPartId);

        return view('courses.lessonParts', [
            'lesson' => $lesson,
            'mainPart' => $mainPart
        ]);
    }
}
