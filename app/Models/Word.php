<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 * @property int $id
 * @property int $image_id
 * @property int $language_id
 * @property string $word
 * @method static Word inRandomOrder()
 */

class Word extends Model
{
    use HasFactory;

    /**
     * @var int|mixed
     */
    private $image_id;

    public function translations()
    {
        return $this->belongsToMany(Word::class,
            'words_pivot',
            'node_word_id',
            'related_word_id',
        );
    }

    public function examples()
    {
        return $this->hasMany(Example::class);
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'word_id');
    }

    public function transcriptions()
    {
        return $this->hasMany(Transcription::class, 'word_id');
    }
}
