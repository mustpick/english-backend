<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 * @property int $id
 * @property int $lesson_id
 * @property int $sort
 * @property string $name
 * @property string $html
 */

class LessonPart extends Model
{
    use HasFactory;
}
