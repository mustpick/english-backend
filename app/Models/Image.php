<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Image
 * @package App\Models
 * @property int $id
 * @property int $image_id
 * @property string $image_url
 * @property int $word_id
 */

class Image extends Model
{
    use HasFactory;
}

