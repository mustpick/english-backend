<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 * @property int $id
 * @property int $lesson_part_id
 * @property string $name
 * @property boolean $type
 */

class Quiz extends Model
{
    use HasFactory;
}
