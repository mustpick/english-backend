<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 * @property int $id
 * @property string $dic_name
 *  @property int $user_id
 */

class Dictionary extends Model
{
    use HasFactory;

    public function dictionaries(){
        $this->hasOne(Dictionary::class, 'user_id');
    }

    public function words()
    {
        return $this->belongsToMany(Word::class,
            'dictionaries_pivot',
            'dictionary_id',
            'word_id',
        );
    }

    public function random_word(){
        return $this->words()->with('translations', 'image', 'transcriptions')->inRandomOrder()->first();
    }

}
