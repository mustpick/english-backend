<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property string $image_url
 * @property string $name
 * @property string $description
 */
class Course extends Model
{
    use HasFactory;

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'course_id');
    }
}
