<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 * @property int $id
 * @property int $course_id
 * @property string $name
 * @property int $sort
 */

class Lesson extends Model
{
    use HasFactory;

    public function parts()
    {
        return $this->hasMany(LessonPart::class, 'lesson_id');
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
