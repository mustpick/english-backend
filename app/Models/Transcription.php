<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 * @property int $id
 * @property string $pronunciation
 * @property string $accent
 * @property string $audio
 * @property int $word_id
 */

class Transcription extends Model
{
    use HasFactory;

}
