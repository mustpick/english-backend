<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @package App\Models
 * @property int $id
 * @property int $quiz_id
 * @property string $text
 * @property boolean $is_correct
 */

class QuizOption extends Model
{
    use HasFactory;
}
