<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 * @property int $id
 * @property string $example
 * @property int $word_id
 */

class Example extends Model
{
    use HasFactory;

    public function word()
    {
        return $this->hasOne(Word::class);
    }
}
