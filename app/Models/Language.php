<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Word
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $country_id
 */

class Language extends Model
{
    use HasFactory;

    public function country()
    {
        return $this->hasOne(Country::class);
    }
}
