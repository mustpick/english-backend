<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>

<style>
    .word{
        text-align: center;
    }

    .visible-invisible{
        opacity: 0;
    }

    .audio{
        margin-left: 20px;
    }

    .word_image {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        max-width: 500px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div>
                <p><img class="word_image" src="{{$word->image->image_url}}"></p>
            </div>
            <div class="word_pronounce">
                @foreach ($word->transcriptions as $pronunciation)
                    <h3>{!! $pronunciation->pronunciation !!}<span>/{{$pronunciation->accent}}/</span><span class="audio"><audio controls class="audio"><source src="{{$pronunciation->audio}}"></audio></span></h3><br>
                @endforeach
            </div>
            <div class="word">
                <h1>{{ $word->word }}<span id="visible-invisible0" class="visible-invisible"> - {{ $word->translations[0]->word }}</span></h1>
            </div>
        </div>
        <div class="col-md-12">
            <div class="word" style="margin-top: 50px">
                <div id="examples" class="collapse">
                    @foreach ($word->examples as $example)
                        <p>{!! $example->example !!}</p>
                    @endforeach
                </div>
                <a href="{{ route('guess-word', $word->pivot->dictionary_id) }}" type="button" class="btn btn-success">Следующее слово</a>
                <a href="#" id="show_translation" type="button" class="btn btn-primary">Показать перевод</a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    let el = document.getElementById('show_translation');
    el.onclick = function(el){
        document.getElementById('visible-invisible0').style.opacity = "1";
        $('#examples').collapse('toggle');
        return false
    };
</script>
