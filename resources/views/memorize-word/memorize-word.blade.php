<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>

<style>
    .word_image {
        display: block;
        margin-left: auto;
        margin-right: auto;
        width: 100%;
        max-width: 500px;
    }

    #heading {
        text-align: center;
    }

    #selection_block {
        text-align: center;
    }

    #next_word {
        margin-top: 30px;
    }

    #choice_word_btn {
        margin-top: 30px;
    }
</style>

<div>
    <image class="word_image" src="{{ $wordAndAnswer[$rand_key = array_rand($wordAndAnswer)]->image->image_url }}"></image>
</div>
<div>
    <h4 id="heading">
        Выберете правильный перевод к слову<br> - <br><span>{{ $wordAndAnswer[$rand_key]->word }}</span>
    </h4>
</div>
<fieldset id="selection_block">
    @foreach($wordAndAnswer as $answer)
        <div>
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
            <label class="form-check-label" for="gridRadios1">
                {!! $answer->translations[0]->word !!}
            </label>
        </div>
    @endforeach
        <button type="submit" class="btn btn-outline-info" id="choice_word_btn">Выбрать</button>
        <a href="{{ route('memorize-word') }}" type="button" class="btn btn-outline-success" id="next_word">Следующее слово</a>
</fieldset>


