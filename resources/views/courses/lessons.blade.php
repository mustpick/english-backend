<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="list-group">
                @foreach ($course->lessons as $lesson)
                    <a href="{{route('course.lesson.parts', ['courseId' => $course->id, 'lessonId' => $lesson->id])}}" class="list-group-item list-group-item-action">{{$lesson->name}}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
</body>
