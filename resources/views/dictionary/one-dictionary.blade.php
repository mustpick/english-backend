<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"></script>

<style>
    #create_name {
        margin-top: 20px;
        margin-left: 20px;
    }

    #dictionary_name  {
        margin-left: 20px;
        width: 100%;
        max-width: 500px;
    }
    #dictionaries {
        margin-left: 20px;
        width: 100%;
        max-width: 500px;
    }
    #delete_dictionary_btn {
        margin-top: 25px;
        margin-left: 20px;
    }

    #update_dictionary_btn {
        margin-top: 25px;
        margin-left: 20px;
    }
    #dictionary_list {
        margin-top: 25px;
        margin-left: 20px;
    }
    .btn-outline-primary {
        margin-left: 20px;
    }
</style>

<div>
    <form id="data" method="POST" action="{{ route('dictionary-update', $dictionary->id)}}">
        @csrf

        <label for="name" class="" id="create_name">Поменяйте имя словаря</label>
        <input type="text" class="form-control" id="dictionary_name" name="name" placeholder="Введите имя словаря" value="{{ $dictionary->dictionary_name }}"><br>

        <a href="{{ route('dictionary-delete', $dictionary->id) }}" type="button" class="btn btn-outline-danger" id="delete_dictionary_btn">Удалить словарь</a>
        <button type="submit" class="btn btn-outline-info" id="update_dictionary_btn">Обновить словарь</button>
    </form>
    <a href="{{ route('guess-word', $dictionary->id) }}" type="button" class="btn btn-outline-primary">Играть в слова</a>
</div>

