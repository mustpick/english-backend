<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code', 3);
            $table->timestamps();
        });

        Schema::create('languages', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->nullable()->references('id')->on('countries');
            $table->string('name');
            $table->string('code', 2);
            $table->timestamps();
        });

        Schema::create('words', function (Blueprint $table) {
            $table->id();
            $table->foreignId('language_id')->nullable()->references('id')->on('languages');
            $table->string('word');
            $table->timestamps();
        });

        Schema::create('words_pivot', function (Blueprint $table) {
            $table->id();
            $table->foreignId('node_word_id')->references('id')->on('words');
            $table->foreignId('related_word_id')->references('id')->on('words');
        });

        Schema::create('examples', function (Blueprint $table) {
            $table->id();
            $table->foreignId('word_id')->references('id')->on('words');
            $table->string('example');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words_pivot');
        Schema::dropIfExists('words');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('examples');
    }
}
