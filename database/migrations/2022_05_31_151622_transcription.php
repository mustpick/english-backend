<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transcription extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transcriptions', function (Blueprint $table) {
            $table->id();
            $table->text('pronunciation')->nullable();
            $table->text('accent')->nullable();
            $table->text('audio')->nullable();
            $table->foreignId('word_id')->nullable()->references('id')->on('words');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('transcriptions');
    }
}
