<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDictionary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dictionaries', function (Blueprint $table) {
            $table->id();
            $table->string('dictionary_name');
            $table->foreignId('user_id')->nullable()->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('dictionaries_pivot', function (Blueprint $table) {
            $table->id();
            $table->foreignId('dictionary_id')->nullable()->references('id')->on('dictionaries');
            $table->foreignId('word_id')->nullable()->references('id')->on('words');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dictionary');
    }
}
