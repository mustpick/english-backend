<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Course;
use App\Models\Example;
use App\Models\Image;
use App\Models\Language;
use App\Models\Lesson;
use App\Models\LessonPart;
use App\Models\Transcription;
use App\Models\User;
use App\Models\Word;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;


class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $user = User::factory()->make();
        $user->save();

        $course = new Course();
        $course->user_id = $user->id;
        $course->name = $faker->name;
        $course->image_url = 'https://cdnn21.img.ria.ru/images/07e4/06/1d/1573629707_0:160:3072:1888_1920x0_80_0_0_a5efd98be7a23d7889dac0a8f93d82af.jpg.webp';
        $course->description = $faker->realText;
        $course->save();
        $lessonSort = 0;

        foreach (Lesson::factory()->count(10)->make() as $lesson)
        {
            /** @var Lesson $lesson */
            $lesson->sort = $lessonSort++;
            $lesson->course_id = $course->id;
            $lesson->save();
            $lessonPartSort = 0;
            foreach (LessonPart::factory()->count(8)->make() as $lessonPart)
            {
                /** @var LessonPart $lessonPart */
                $lessonPart->sort = $lessonPartSort++;
                $lessonPart->lesson_id = $lesson->id;
                $lessonPart->save();
            }
        }
    }
}
