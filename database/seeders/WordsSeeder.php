<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Example;
use App\Models\Image;
use App\Models\Language;
use App\Models\Transcription;
use App\Models\Word;
use Illuminate\Database\Seeder;

class WordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $words = [
            'ask smb out' => [
                'translation' => 'пригласить на свидание',
                'examples' => [
                    'John <b>asked</b> Nancy <b>out</b> to (for) dinner —  Джон <b>пригласил</b> Нэнси на <b>обед</b>',
                    'Did that nice young man <b>ask</b> you <b>out</b>? — Этот приятный молодой человек <b>пригласил</b> тебя <b>на свидание?</b>'
                ],
                'image_url' => 'https://psyfactor.org/lib/i/xsvidanie2.jpg.pagespeed.ic.PmhECNsFWo.webp',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                ]
            ],
            'ask around' => [
                'translation' => 'спрашивать',
                'examples' => [
                    'I <b>asked</b> <b>around</b> but nobody knew how to find that hotel — Я <b>поспрашивал</b> у людей, но никто не знает, как найти этот отель',
                    'What? Sorry, I haven’t seen your cat. <b>Ask around</b>. — Что? Простите, я не видел вашу кошку. </b>Поспрашивайте у людей<b>'
                ],
                'image_url' => 'https://www.mordovmedia.ru/static/644a1458cc516a7ac315d21b3a319977/thumbs/media/articles/1272/a46f57c512c3acde17cdd8a9bd938845.jpg/660x372.jpg',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'be after' => [
                'translation' => 'пытаться что-то получить, найти',
                'examples' => [
                    'What <b>are</b> you <b>after</b> in that room? There’s nothing in there — Что ты пытаешься <b>найти</b> в этой комнате? Здесь ничего нет',
                    'I don’t know what he <b>is</b> <b>after</b> — Я не знаю, что ему <b>нужно</b>'
                ],
                'image_url' => 'https://english-abc.ru/img/lesson_theory/look/3.jpg',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
            ]
                ],
            'be away' => [
                'translation' => 'находиться в другом месте',
                'examples' => [
                    'The Johnson’s <b>were away</b> all last week to Mexico — Семья Джонсонов всю прошлую неделю <b>была в отъезде</b> в Мексике',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
            ]
                ],
            'blow up' => [
                'translation' => 'взрываться',
                'examples' => [
                    'Cars don’t <b>blow up</b> like they do in movies — Машины не <b>взрываются</b>, как в кино',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'break up' => [
                'translation' => 'расстаться',
                'examples' => [
                    'Jack and Helen <b>broke up</b> finally — Джек и Элен наконец-то <b>расстались</b>',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'break down' => [
                'translation' => 'сломаться',
                'examples' => [
                    'Can you give me a ride? My car <b>broke down</b> — Можете меня подвезти? Моя машина <b>сломалась</b>',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'break in' => [
                'translation' => 'вломиться',
                'examples' => [
                    'The police <b>broke in</b> and arrested everyone — Полицейские <b>вломились</b> и всех арестовали',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'break out' => [
                'translation' => 'сбежать',
                'examples' => [
                    'The movie is about a guy who <b>broke out</b> of jail — Фильм о парне, <b>сбежавшем</b> из тюрьмы',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'bring along' => [
                'translation' => 'привести кого-то с собой',
                'examples' => [
                    'He <b>brought along</b> his son to the football match — Он <b>привел с собой</b> сына на футбольный матч',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'bring up' => [
                'translation' => 'упомянуть, воспитывать',
                'examples' => [
                    'I didn’t want to <b>bring up</b> business at lunch — Я не хотел <b>упоминать</b> о делах за обедом',
                    'His grandmother <b>brought</b> him <b>up</b> — Его растила бабушка '
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'call by' => [
                'translation' => 'навестить',
                'examples' => [
                    'I wanted to <b>call by</b> on my way home — Я хотел <b>зайти</b> к тебе по дороге домой',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'calm down' => [
                'translation' => 'успокоиться, успокоить кого-то',
                'examples' => [
                    '<b>Calm down</b>, everything is going to be just fine — <b>Успокойтесь</b>, все будет просто прекрасно',
                    'The nurse came up to the little girl and <b>calmed</b> her <b>down</b> – Медсестра подошла к маленькой девочке и <b>успокоила</b> ее'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'chip in' => [
                'translation' => 'скинуться деньгами',
                'examples' => [
                    'I’m gonna order a pizza, let’s <b>chip in</b> — Я закажу пиццу, давайте <b>скинемся</b>',
                    'They each <b>chipped in</b> ten dollars to buy a present – Они все <b>скинулись</b> по 10 долларов, чтобы купить подарок '
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'count on' => [
                'translation' => 'полагаться на кого-то',
                'examples' => [
                    'You can <b>count on</b> my friend, he always keeps his word — Вы можете <b>положиться на моего друга</b>, он всегда держит слово'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'check in/out' => [
                'translation' => 'поселиться, выселиться из гостиницы',
                'examples' => [
                    'We <b>checked in</b> on Saturday, and we <b>check out</b> on Tuesday — Мы <b>поселимся (в гостинице)</b> в субботу, а <b>съедем</b> во вторник'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'check with' => [
                'translation' => 'согласовать с кем-то',
                'examples' => [
                    'He needs to <b>check with</b> his wife to make sure they don’t have other plans — Ему нужно <b>посоветоваться (согласовать)</b> с женой, чтобы убедиться, что у них нет других планов'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come across' => [
                'translation' => 'наткнуться на что-то, кого-то',
                'examples' => [
                    'I <b>came across</b> my ex-wife in the grocery store — Я случайно <b>наткнулся на бывшую жену</b> в продуктовом магазине'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come up to' => [
                'translation' => 'подойти к кому-то, чему-то',
                'examples' => [
                    'She <b>came up to me</b> and asked if I was lost — Она <b>подошла ко мне</b> и спросила не заблудился ли я',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come up with' => [
                'translation' => 'придумывать решение',
                'examples' => [
                    'And then all of a sudden Mary <b>came up with</b> her brilliant plan — А затем совершенно внезапно Мэри <b>придумала</b> свой блестящий план',
                    'Just <b>come up with</b> something – Просто <b>придумай</b> что-нибудь (решение)'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come from' => [
                'translation' => 'быть откуда-то родом',
                'examples' => [
                    'Where do you <b>come from?</b> — <b>Откуда </b>ты?',
                    'She <b>comes from</b> Spain – Она <b>из</b> Испании'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come off' => [
                'translation' => 'отваливаться',
                'examples' => [
                    'Old paint has <b>come off</b> the wall — Старая краска <b>отвалилась</b> от стены'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come over' => [
                'translation' => 'прийти к кому-то',
                'examples' => [
                    'My parents are gone for a business trip, <b>come over</b> — Мои родители уехали в командировку, <b>приходи ко мне</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'come around' => [
                'translation' => 'Навестить, приходить в себя после потери сознания',
                'examples' => [
                    'I live just across the street, <b>come around</b> some time — Я живу через дорогу, </b>заходи<b> как-нибудь',
                    'He was unconscious but the doctor made him come around – Он был без сознания, но доктор <b>привел его в себя</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'cut down on' => [
                'translation' => 'сократить расход чего-то',
                'examples' => [
                    'We’ll have to <b>cut down on</b> water if we want to last until help arrives — Нам придется <b>снизить расход воды</b>, если мы хотим протянуть до прибытия помощи',
                    'The gevernment is going to <b>cut down on</b> defence spending – Правительство <b>собирается сократить расходы</b> на оборону'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'cut smt off' => [
                'translation' => 'отрезать что-то, изолировать',
                'examples' => [
                    'Why did you <b>cut the sleeves off</b>? — Зачем вы <b>отрезали рукава</b>?',
                    'Ton this island, we are <b>cut off from the rest of the world</b> – На этом острове мы <b>отрезаны от остального мира</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'cut smt out' => [
                'translation' => 'вырезать что-то',
                'examples' => [
                    'She <b>cut out his picture</b> from the magazine — Она <b>вырезала его фотографию</b> из журнала',
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'cut in' => [
                'translation' => 'подрезать на автомобиле',
                'examples' => [
                    'The green Ford <b>cut in</b>front of us as if he owned the road! — Зеленый Форд нас <b>подрезал</b>, как будто это его дорога!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'deal with' => [
                'translation' => 'вести дела',
                'examples' => [
                    'I prefer to <b>deal with</b> the same representative each time — Я предпочитаю каждый раз <b>вести дела</b> с тем же представителем'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'dress up' => [
                'translation' => 'приодеться',
                'examples' => [
                    'You don’t have to <b>dress up</b> to go to the mall, jeans and a T-shirt a fine — Тебе не нужно <b>наряжаться</b> для торгового центра, джинсы и футболка пойдут',
                    'Ellie <b>dressed up</b> as a witch for Halloween – Элли <b>переоделась</b> в ведьму на Хэллоуин'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'end up' => [
                'translation' => 'оказаться в итоге',
                'examples' => [
                    'That’s how i <b>ended up</b> in this small town — Вот как я <b>в итоге оказался</b> в этом городе',
                    'After such a brilliat career, he <b>ended up</b> selling second hand cars – После такой блестящей карьеры, он <b>в итоге стал</b> продавцом подержанных автомобилей'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'fall down' => [
                'translation' => 'падать',
                'examples' => [
                    'My cat <b>fall down</b> from the balcony, but it’s ok — Мой кот <b>упал</b> с балкона, но он в порядке'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'fall for smd' => [
                'translation' => 'влюбляться',
                'examples' => [
                    'Mike <b>fell for Jane</b> — Майк <b>влюбился</b> в Джейн'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'fall for smt' => [
                'translation' => 'купиться на уловку',
                'examples' => [
                    'That’s a stupid story, my wife will never <b>fall for it</b> — Это глупая история, моя жена на такое никогда не <b>купится</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'fall behind' => [
                'translation' => ' отставать',
                'examples' => [
                    'One of the tourists <b>fell behind</b> and got lost — Один из туристов <b>отстал</b> и потерялся',
                    'We have to hurry, we are <b>falling behind</b> the schedule – Нам нужно поторопиться, мы <b>отстаем</b> от графика'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'fill in/out' => [
                'translation' => 'заполнить (бланк)',
                'examples' => [
                    'There will be a lot of paperwork, you’ll have to read, <b>fill in</b>, sign hundreds of documents — Будет много бумажной работы, тебе придется прочитать, <b>заполнить</b>, подписать сотни документов'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'figure out' => [
                'translation' => ' узнать, выяснить',
                'examples' => [
                    'I dont’ know how it works but let’s <b>figure</b> it <b>out</b> — Не знаю, как это работает, но давай это <b>выясним</b>',
                    'How did you <b>find out</b> where to find me? – Как вы <b>выяснили</b>, где меня найти?'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get along with' => [
                'translation' => 'ладить с кем-то',
                'examples' => [
                    'In school, i didn’t <b>get along with</b> my classmates — В школе я не <b>ладил</b> с одноклассниками'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get through' => [
                'translation' => 'дозвониться по телефону',
                'examples' => [
                    'I called you twice but couldn’t <b>get through</b> — Я два раза вам звонил, но не мог <b>дозвониться</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get in' => [
                'translation' => 'садиться в машину',
                'examples' => [
                    'Hey, we gotta hurry! <b>Get in!</b> — Эй, нам нужно торопиться! <b>Садись в машину!</b>',
                    'He didn’t see the truck coming when we was <b>getting in</b> his car – Он не видел, что приближается грузовик, когда <b>садился в машину</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get on' => [
                'translation' => 'садиться в поезд, самолет, судно, автобус',
                'examples' => [
                    'I am afraid, we <b>got on</b> the wrong train — Боюсь, что мы <b>сели не на тот поезд</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get off' => [
                'translation' => 'cходить с транспорта, снять с чего-то',
                'examples' => [
                    'I’m <b>getting off</b> here, see you later! — Я здесь <b>выхожу</b>, увидимся!',
                    '<b>Get</b> your feet <b>off</b> my table! – <b>Убери</b> свои ноги с моего стола!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get up/down' => [
                'translation' => 'подниматься, вставать / падать, пригибаться',
                'examples' => [
                    'The boxer <b>got up</b> and continued to fight — Боксер <b>поднялся</b> и продолжил бой',
                    'When something exploded i <b>got down</b>, but it was just a firework – Когда что-то взорвалось, я <b>пригнулся</b>, но это был всего лишь фейерверк'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'get away with smt' => [
                'translation' => 'избежать наказания за что-то',
                'examples' => [
                    'How to <b>get away with murder</b> — Как <b>избежать наказания</b> за убийство'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'give up' => [
                'translation' => 'сдаваться, прекращать что-то делать',
                'examples' => [
                    'Fight and never <b>give up</b> — Дерись и никогда не <b>сдавайся</b>',
                    'I <b>gave up</b> smoking – Я <b>бросил (прекратил)</b> курить'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'give smt away' => [
                'translation' => 'выдавать тайну, раздавать',
                'examples' => [
                    'Someone <b>gave</b> your little <b>secret away</b> — Кто-то <b>рассказал о</b> твоем маленьком <b>секрете</b>',
                    'The are <b>giving away</b> some unsold <b>stuff</b> – Они <b>раздают</b> какие-то нераспроданные <b>вещи</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'give back' => [
                'translation' => 'возвращать',
                'examples' => [
                    'You took my phone! <b>Give</b> it <b>back</b>! — Ты взял мой телефон! <b>Верни</b> его!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'give out' => [
                'translation' => 'раздавать',
                'examples' => [
                    'You can’t just <b>give out</b> the candies, they are one dollar each — Ты не можешь просто так <b>раздавать</b> конфеты, они стоят по доллару за штуку'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go out' => [
                'translation' => ' ходить куда-нибудь',
                'examples' => [
                    'I <b>go out</b> with my friends every Friday night — Я <b>хожу куда-нибудь</b> с друзьями вечером каждую пятницу'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go out with smb' => [
                'translation' => 'встречаться с кем-нибудь',
                'examples' => [
                    'Are you still <b>going out with Bob</b>? — Ты все еще <b>встречаешься с Бобом?</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go with' => [
                'translation' => 'подходить, сочетаться',
                'examples' => [
                    'These shoes don’t <b>go</b> well <b>with</b> your pants — Эти туфли плохо <b>сочетаются с твоими брюками</b>',
                    'What wine goes with fish? – Какой вино подходит к рыбе?'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go back to' => [
                'translation' => 'возвращаться к какому-то занятию',
                'examples' => [
                    'We <b>went back to</b> work after a short break — Мы <b>вернулись к</b> работе после короткого перерыва'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go down/up' => [
                'translation' => 'сокращаться/увеличиваться',
                'examples' => [
                    'Are you expecting the prices to <b>go down</b>? Normally, they only <b>go up</b> — Ты ожидаешь, что цены <b>упадут</b>? Обычно они только <b>растут</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'go without smt' => [
                'translation' => 'обходиться, справляться без чего-то',
                'examples' => [
                    'This time you’ll have to <b>go without my help</b> — На этот раз тебе придется обойтись <b>без моей помощи</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hand out' => [
                'translation' => 'раздавать группе людей',
                'examples' => [
                    '<b>Hand out</b> the invitations to everyone — <b>Раздайте</b> всем приглашения'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hand in' => [
                'translation' => 'сдавать',
                'examples' => [
                    'You have to <b>hand in</b> your essay by Monday — Вы должны <b>сдать</b> сочинение до понедельника'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'grow up' => [
                'translation' => 'расти, стать взрослым',
                'examples' => [
                    'When i <b>grow up</b>, i want to be a doctor — Я хочу стать врачом, когда <b>вырасту</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'grow back' => [
                'translation' => 'отрасти, вырасти заново',
                'examples' => [
                    'Don’t worry about your haircut, it’ll <b>grow back</b> — Не беспокойся насчет своей стрижки, волосы <b>отрастут</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'grow out of smt' => [
                'translation' => 'вырасти из чего-то',
                'examples' => [
                    'My kids <b>grew out of the clothes</b> i had bought just a few months ago — Мои дети <b>выросли из одежды</b>, которую я купила всего несколько месяцев назад',
                    'I <b>grew out of cartoons</b> – Я уже слишком <b>взрослый для мультиков</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hang in' => [
                'translation' => 'держаться, не падать духом',
                'examples' => [
                    '<b>Hang in</b> there! We’re coming to rescue you — <b>Держитесь</b>! Мы идем на помощь'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hang out' => [
                'translation' => ' тусоваться с кем-то, проводить время',
                'examples' => [
                    'I’m gonna <b>hang out</b> with my friends today — Я сегодня собираюсь <b>потусоваться</b> с друзьями'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hang up' => [
                'translation' => 'повесить трубку',
                'examples' => [
                    'Wait! Don’t <b>hang up</b>! — Подожди! Не <b>вешай трубку!</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hold on' => [
                'translation' => ' просьба подождать, держаться',
                'examples' => [
                    '<b>Hold on</b>, I forgot my phone — <b>Подожди</b>, я забыл телефон',
                    '<b>Hold on</b>, guys, help is coming – <b>Держитесь</b>, ребята, помощь уже в пути'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hold it against smb' => [
                'translation' => 'держать зло на кого-то',
                'examples' => [
                    'He lied to me but I don’t <b>hold it against him</b> — Он мне солгал, но я не <b>держу на него зла</b> за это'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hold back' => [
                'translation' => 'сдерживать физически',
                'examples' => [
                    'A seven nation army couldn’t <b>hold</b> me <b>back</b> — Армия семи народов (стран) не могла меня <b>сдержать</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'hurry up' => [
                'translation' => 'торопиться',
                'examples' => [
                    'You have to <b>hurry up</b>, we are almost late — Тебе нужно <b>торопиться</b>, мы почти опоздали'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'keep (on) doing smt' => [
                'translation' => 'продолжать что-то делать',
                'examples' => [
                    '<b>Keep on stirring</b> until it boils — <b>Продолжайте помешивать</b> пока не закипит',
                    '<b>Keep going</b>, <b>keep going</b> – <b>Вперед</b>, <b>вперед</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'keep smt from smb' => [
                'translation' => 'держать что-то в тайне от кого-то',
                'examples' => [
                    'You can’t <b>keep</b> your <b>disease from your</b> family — Ты не можешь <b>скрывать свою болезнь от семьи</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'keep smt/smb out' => [
                'translation' => 'не давать приблизиться',
                'examples' => [
                    'You should <b>keep</b> your <b>dog out</b> of my lawn — Тебе лучше <b>держать</b> свою <b>собаку подальше</b> от моего газона',
                    '<b>Keep</b> your <b>hands out</b> of me! – <b>Держи</b> свои <b>руки</b> от меня <b>подальше</b>!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'let smb down' => [
                'translation' => 'подвести',
                'examples' => [
                    'Don’t worry, you can rely on me, I won’t <b>let you down</b> — Не беспокойся, ты можешь на меня положиться. Я тебя не <b>подведу</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'let smb in' => [
                'translation' => 'впустить, пропусти',
                'examples' => [
                    'Guy, <b>let me in</b>, it’s cold out there! — Ребята, <b>впустите</b> меня, там холодно!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'look for' => [
                'translation' => 'искать',
                'examples' => [
                    'I am <b>looking for</b> a post office — Я и<b>щу</b> почтовое отделение'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'look forward to' => [
                'translation' => 'ждать с нетерпением чего-то',
                'examples' => [
                    'We are <b>looking forwad to</b> your next visit — Мы <b>с нетерпением ждем</b> вашего следующего визита',
                    'We are <b>looking forward to</b> visiting you – Мы <b>ждем с нетерпением</b>, когда посетим вас'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'look after' => [
                'translation' => 'приглядывать, присматривать',
                'examples' => [
                    'Can you <b>look after</b> my stuff, please? I’ll be right back — Не могли бы вы <b>присмотреть</b> за моими вещами, пожалуйста? Я сейчас вернусь'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'look up' => [
                'translation' => 'найти информацию',
                'examples' => [
                    'I don’t know this word, <b>look</b> it <b>up</b> in the dictionary — Я не знаю такое слово, <b>посмотри</b> в словаре'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'look out' => [
                'translation' => 'опасаться чего-то',
                'examples' => [
                    '<b>Look out</b>! Someone’s coming! — <b>Берегись</b>! Кто-то идет!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'make smt up' => [
                'translation' => 'выдумать, солгать о чем-то',
                'examples' => [
                    'I had to <b>make up a story</b> about why I was late —  Мне пришлось <b>сочинить историю</b> о том, почему я опоздал',
                    'I told you she <b>made it up</b>! – Говорил же я тебе, что она все <b>это выдумала</b>!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'make out' => [
                'translation' => 'страстно и долго целоваться',
                'examples' => [
                    'Jack cought his girlfriend <b>making out</b> with his friend — Джек застал свою девушку, <b>целующейся</b> с его другом'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'move in (to)' => [
                'translation' => 'поселяться в новом жилье, заезжать',
                'examples' => [
                    'We <b>moved in</b> yesterday and know no one here — Мы <b>переехали</b> сюда вчера и никого здесь не знаем',
                    'I’m going to <b>move in to</b> my friend’s place – Я собираюсь <b>переехать к</b> другу'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'move away (to)' => [
                'translation' => 'уезжать откуда-то, съездать с жилья',
                'examples' => [
                    'The Patterson’s have <b>moved away</b>, but I can give your their new address — Паттерсоны <b>съехали</b> (<b>переехали</b>), но я могу дать вам их новый адрес',
                    'I was born in Germany but we <b>moved away to</b> England, when I was a kid – Я родился в Германии, но мы <b>переехали в</b> Англию, когда я был ребенком'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'move on' => [
                'translation' => 'двигаться дальше',
                'examples' => [
                    'I think we’ve talked enough about it, let’s <b>move on</b> — Думаю, мы достаточно об этом поговорили, давайте уже <b>дальше</b>',
                    'I want to change my job, I need to <b>move on</b> – Я хочу сменить работу, мне нужно <b>двигаться дальше</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pass away' => [
                'translation' => 'уходить в мир иной, умирать',
                'examples' => [
                    'My grandfather <b>passed away</b> when I was ten — Мой дедушка <b>покинул нас</b>, когда мне было десять'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pass by' => [
                'translation' => 'проходить, проезжать мимо и не остановиться',
                'examples' => [
                    'We were <b>passing by</b> the City Hall, when Ann saw Harry in the street — Мы <b>проезжали мимо</b> мэрии, когда Энн увидела на улице Гарри'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pass out' => [
                'translation' => 'терять сознание',
                'examples' => [
                    'It was hot in the church and an old lady <b>passed out</b> — В церкви было жарко, и пожилая женщина <b>упала в обморок</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pay smb back' => [
                'translation' => 'вернуть долг, отплатить',
                'examples' => [
                    'Morgan bought me a ticket, but I haven’t <b>paid him back</b> yet — Морган купил мне билет, но я еще не <b>вернул ему деньги</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pay off' => [
                'translation' => 'окупиться',
                'examples' => [
                    'Your effort will <b>pay off</b> — Твои труды <b>окупятся</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'play along with smb' => [
                'translation' => 'подыгрывать',
                'examples' => [
                    'Jim <b>played along with Ron</b>, when he said he was a movie producer — Джим <b>подыграл Рону</b>, когда тот сказал, что он кинопродюсер',
                    'Don’t worry, just <b>play along</b>, ok? – Не волнуйся, <b>просто подыгрывай</b>, хорошо?'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'play around' => [
                'translation' => 'дурачиться',
                'examples' => [
                    'Aren’t you too big boys for <b>playing around</b>? — Разве вы не слишком большие ребята для того, чтобы <b>дурачиться</b>?',
                    'The teacher was angry because we were <b>fooling around</b> – Учитель рассердился, потому что мы <b>дурачились</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pull over' => [
                'translation' => 'остановить машину у дороги',
                'examples' => [
                    'We <b>pulled over</b> to check our tires —  Мы <b>остановились у дороги</b>, чтобы проверить колеса'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'pull oneself together' => [
                'translation' => 'собраться',
                'examples' => [
                    'Come on, <b>pull yourself together</b>, we have to work — Давай уже, <b>соберись</b>, нам нужно работать'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'put on' => [
                'translation' => 'надеть',
                'examples' => [
                    '<b>Put</b> your hat <b>on</b> — <b>Наденьте</b> шляпу',
                    '<b>Put on</b> your seat belts – <b>Пристегните</b> (<b>наденьте</b>) ремни безопасности'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'run away' => [
                'translation' => 'убегать',
                'examples' => [
                    'Tell me them that funny story how you <b>ran away</b> from a dog —  Расскажи им эту забавную историю, как ты <b>убежал</b> от собаки'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'run for' => [
                'translation' => 'догонять, бежать за чем-то',
                'examples' => [
                    'I lost my wallet when was <b>running for</b> a bus —  Я потерял бумажник, когда <b>бежал за</b> автобусом'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'run around' => [
                'translation' => 'быть очень занятым',
                'examples' => [
                    'After <b>running around</b> all day, James is too tired to play with his kids — После того как он <b>занимался делами</b> весь день, Джеймс слишком устал, чтобы поиграть с детьми'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'run on smt' => [
                'translation' => 'работать на чем-то',
                'examples' => [
                    'Does this bus </b>run on gas<b> or <b>electricity</b>? — Этот автобус <b>работает на бензине</b> или <b>электричестве?</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'run over smt/smb' => [
                'translation' => 'переехать на машине',
                'examples' => [
                    'The deer was <b>ran over by a car</b> — Оленя <b>переехала машина</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'set smt up' => [
                'translation' => 'устроить / подставлять',
                'examples' => [
                    'Can you <b>set up a meeting</b> with him? — Вы можете <b>устроить с ним встречу</b>?',
                    'The police have <b>set him up</b>. They put some drugs in his pocket – Полиция <b>его подставила</b>. Они подкинули ему наркотики в карман'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'show off' => [
                'translation' => 'хвастаться',
                'examples' => [
                    'FHe bought the most expensive guitar to <b>show off</b> to his friends — Он купил самую дорогую гитару, чтобы <b>хвататься</b> перед друзьями'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'show up' => [
                'translation' => 'появиться, прийти',
                'examples' => [
                    'We’ve been waiting for him for an hour but he didn’t <b>show up</b> — Мы ждали его целый час, но он не <b>пришел</b>',
                    'He <b>showed up</b> in the middle of the night – Он <b>заявился</b> посреди ночи'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'sleep over' => [
                'translation' => 'переночевать у кого-то в гостях',
                'examples' => [
                    'It’s too late to go back home, why don’t you <b>sleep over</b>? — Уже слишком поздно возвращаться домой, почему бы тебе не остаться <b>переночевать</b>?',
                    'Can i <b>sleep over</b> at my friend’s house? – Могу я <b>переночевать у друга дома</b>?'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'slow down' => [
                'translation' => 'снизить скорость',
                'examples' => [
                    'The car <b>slowed down</b> passing us by — Машина <b>снизила скорость</b>, проезжая мимо нас'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'stand out' => [
                'translation' => 'выделяться, отличаться',
                'examples' => [
                    'You are not supposed to write the best essay ever, but it has to <b>stand out</b> — Ты не должен писать лучшее эссе во все времена, но оно должно чем-то <b>отличаться</b>',
                    'The tourist guide was wearing an orange jacket so that he <b>stood out</b> in a crowd – Гид был одет в оранжевую жилетку, чтобы <b>выделяться</b> в толпе'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'stick to smt' => [
                'translation' => 'придерживаться чего-то',
                'examples' => [
                    'You can’t lose weight if you don’t <b>stick to the diet</b> — Ты не сможешь похудеть, если не будет <b>придерживаться диеты</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'take smt out' => [
                'translation' => ' вынимать, вытаскивать',
                'examples' => [
                    'He <b>took a large pizza out</b> of the box — Он <b>вытащил большую пиццу</b> из коробки'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'take back' => [
                'translation' => 'забрать обратно',
                'examples' => [
                    'I’d like to <b>take back</b> the bike I gave you yesterday — Я бы хотел <b>забрать обратно</b> велосипед, который я дал тебе вчера',
                    'You can’t just <b>take back</b> what you have said – Ты не можешь просто <b>забрать</b> слова <b>обратно</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'take smb for smb' => [
                'translation' => 'принять кого-то за кого-то, перепутать',
                'examples' => [
                    'You father looks young, I <b>took him for your brother</b> — Твой отец молодо выглядит, я <b>принял его за твоего брата</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'take apart' => [
                'translation' => 'разобрать на части',
                'examples' => [
                    'The boy <b>took apart</b> his toy because he wanted to see what’s inside — Мальчик <b>разобрал</b> свою игрушку, потому что хотел увидеть, что внутри'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'try on' => [
                'translation' => 'примерить',
                'examples' => [
                    'This hat looks great, <b>try</b> it <b>on</b> — Эта шляпа выглядит отлично, <b>примерь</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'turn up/down' => [
                'translation' => 'увеличивать, уменьшать громкость',
                'examples' => [
                    'Could you <b>turn down</b> the volume, please? — Не могли бы вы <b>уменьшить громкость</b>, пожалуйста?',
                    'This is my niece singing, <b>turn</b> it <b>up</b>! – Это моя племянница поет, <b>сделай погромче</b>!'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'turn around' => [
                'translation' => 'оборачиваться',
                'examples' => [
                    'Even if hear something creepy, don’t <b>turn around</b> — Даже если услышишь что-нибудь жуткое, не <b>оборачивайся</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'turn away' => [
                'translation' => 'отворачиваться',
                'examples' => [
                    'Harry stared at her and she <b>turned away</b> — Гарри уставился на нее, и она <b>отвернулась</b>'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
            ],
            'turn into' => [
                'translation' => 'превращаться',
                'examples' => [
                    'In that fairy tale, a boy could <b>turn into</b> a wolf — В этой сказке мальчик умел <b>превращаться в</b> волка'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                    ]
                ],
            'use up smt ' => [
                'translation' => 'истратить',
                'examples' => [
                    'Have you <b>used up all the painkillers</b>? —  Ты уже <b>истратил</b> все обезболивающее?'
                ],
                'image_url' => 'https://sun4-12.userapi.com/s/v1/ig2/GauHB1V099OGcELIx77NhAudltJgn-DKYKkbqejFYEo_M08zQLwws9Tvv1lGYVufbXz6bK0SVPXKThgom2p0J-7p.jpg?size=1280x934&quality=96&type=album',
                'transcription' => [
                    'UK_pronounce' => 'trænˈskrɪpʃən',
                    'US_pronounce' => 'kɒk'
                ]
            ],
        ];

        $england = new Country();
        $england->name = 'England';
        $england->code = 'UK';
        $england->save();

        $english = new Language();
        $english->country_id = $england->id;
        $english->name = 'english';
        $english->code = 'en';
        $english->save();

        foreach ($words as $word => $data) {
            $translationRecord = new Word();
            $translationRecord->word = $data['translation'];
            $translationRecord->save();

            $record = new Word();
            $record->word = $word;
            $record->language_id = $english->id;
            $record->save();
            $record->translations()->sync([$translationRecord->id]);

            foreach ($data['examples'] as $example) {
                $exampleRecord = new Example();
                $exampleRecord->word_id = $record->id;
                $exampleRecord->example = $example;
                $exampleRecord->save();
            }
            if (isset($data['image_url'])) {
                $imageUrlRecord = new Image();
                $imageUrlRecord->image_url = $data['image_url'];
                if (isset($imageUrlRecord)) {
                    $imageUrlRecord->word_id = $record->id;
                }
                $imageUrlRecord->save();
            }

            $UKtranscriptionRecord = new Transcription();
            if (isset($data['transcription']['UK_pronounce'])) {
                $UKtranscriptionRecord->pronunciation = $data['transcription']['UK_pronounce'];
            }
            $UKtranscriptionRecord->accent = 'UK';
            $UKtranscriptionRecord->word_id = $record->id;
            $UKtranscriptionRecord->save();

            $UStranscriptionRecord = new Transcription();
            if (isset($data['transcription']['US_pronounce'])) {
                $UStranscriptionRecord->pronunciation = $data['transcription']['US_pronounce'];
            }
            $UStranscriptionRecord->accent = 'US';
            $UStranscriptionRecord->word_id = $record->id;
            $UStranscriptionRecord->save();
        }
    }
}
