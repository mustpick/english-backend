<?php

namespace Database\Factories;

use App\Models\LessonPart;
use Illuminate\Database\Eloquent\Factories\Factory;

class LessonPartFactory extends Factory
{
    protected $model = LessonPart::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'html' => $this->faker->randomHtml
        ];
    }
}
